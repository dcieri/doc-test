# Suggest new features
When you suggest a feature, please do it one at the time.

Your feature will not be automatically added to the repository, but the Hog group will evaluate it. For this reason, it is important that you describe in detail your suggestion.

Open an issue using the label ![proposal](./figures/proposal.png).

You can do that [at this link](https://gitlab.cern.ch/hog/Hog/issues).


Let us know: 

- if it's a missing feature or a feature that should change
- what will this save or fix
- to which part of Hog this is related to
- how you have been testing it (e.g. operative system, IDE software, Hog version etc.)

Please do not use any other labels apart form "Feature proposal", "Question" and "Problem report" because they are meant for developers rather than users.