# Simulation
Hog supports Vivado Simulator (also called Xsim), Mentor Graphics Questasim  and Mentor Graphics Modelsim.

The simulation setup is always handled by Vivado/Quartus, as if the simulation button on the GUI was clicked, then the simulation run is done with the simulation software.

## Simulation sets
Simulations are organised in so-called "simulation sets" as in Vivado.

![simulation sets](figures/simulation_sets.png)

In this figure you can see an example of simulation sets, these are contained in Vivado in the `Simulation_Sources` folder in the hierarchy view.
You can right click on each of them, set it as active (becomes bold) and run it by clicking the "Run simulation button". In the figure above `algo_sim` is the active simulation.

Every simulation set has a top entity, the test bench containing the device under test, and can be simulated independently.

In Hog every simulation set correspond to a `.sim` [list file](02-List-files.md#simulation-list-files-sim).
In the `.sim` list-files, in addition to the properties defined for the `.src` list files, some other properties are specified.
These properties are:

- `topsim=<entity_name>`: (mandatory) The name of the entity that will be the top level of the simulation;
- `wavefile`: (optional) it indicates the name of the entity you want to set as top level in your simulation (Questasim/Modelsim only);
- `dofile`: (optional) it indicates the file containing the signal waveforms to be observed in your simulation (Questasim/Modelsim only).

## Project simulator
The software used to simulate is set at project level in Vivado [^1]. To change it you have to change the `SIMULATOR` parameter in your [project Tcl file](01-Project-Tcl.md#simulator).
[^1]:Vivado does not allow to run different simulations with different simulators automatically. If you want to do that locally, every time you want to switch from one simulator to another, you have to go to the simulation settings. The setting will be valid at project level, so for all the simulations sets. If you change the simulator by clicking in Vivado GUI, the change will not be propagated to the repository.


## Simulation in the CI
If the simulation job configured in your'projects CI, the
[Hog CI](../02-Hog-CI/01-CI-Introduction.md) will automatically run the simulation sets.

The CI stage will fail if one simulation fails. This means that, in order for the CI stage to be meaningful, each simulation set should be designed to fail if some unwanted behaviour is present.

Even if Vivado doesn't allow for different simulators in the same project, in Hog CI it is possible to specify a different software to be used in each set. This can be done by adding at the top of the `.sim` list one of the following lines:


```
#Simulator xsim     # For Vivado Simulator
#Simulator questa   # For QuestaSim Simulator
#Simulator modelsim # For ModelSim Simulator

#Simulator skip_simulation #To skip the simulation in the CI
```
If you do not specify the simulator software, Hog will set ModelSim as default.

If for any reason you don't want a specific simulation set to be simulated by the CI, just use `skip_simulation`.

An example `.sim` list file looks like this
```
#Simulator Xsim
tb_source_dir/tb_for_lib1.vhd topsim=tb_lib1
wave_source_dir/wave_lib1.tcl wavefile
do_source_dir/dofile_lib1.do dofile
tb_source_dir/another_file.vhd
```

## Simulating without GUI
If you want to run the simulation locally, but without using the GUI (i.e. in the same way that CI does), you can use the `Hog/LaunchSimulation.sh` wrapper script.
```console
Hog/LaunchSimulation.sh <proj_name> [library path]
```
This script will launch all the simulation sets in the specified projects, using the software specified in the `.sim` file and skipping the simulation if `skip_simulation` is specified.

This script will assume that the Modeslim/Questasim libraries are compiled and located in `<repository>/SimulationLib`. If this is not the case, the simulation library path should be specified via the `[library path]` option.
