# Beta Release v1.11.0
https://gitlab.cern.ch/api/v4/projects/74736/jobs/artifacts/refs/merge-requests%2F138%2Fhead/raw/changelog.md?job=changelog
## Repository info
- Merge request number: 138
- Branch name: 94-add-bitfiles-and-log-to-gitlab-releases-2

## Changelog

- New tcl script to get binary files links for releases
- binary files can be downloaded directly from the release
