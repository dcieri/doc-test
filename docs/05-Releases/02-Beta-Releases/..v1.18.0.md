# Beta Release v1.18.0
https://gitlab.cern.ch/api/v4/projects/74736/jobs/artifacts/refs/merge-requests%2F152%2Fhead/raw/changelog.md?job=changelog
## Repository info
- Merge request number: 152
- Branch name: sh-script-update

## Changelog

- copy binary file to proper location on EOS
- Modifyed shell scripts to be fully compatible with supported HLS compilers
