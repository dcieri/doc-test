# Beta Release v1.0.9
https://gitlab.cern.ch/api/v4/projects/74736/jobs/artifacts/refs/merge-requests%2F136%2Fhead/raw/changelog.md?job=changelog
## Repository info
- Merge request number: 136
- Branch name: hotfix-binfile

## Changelog

- (bugfix) In case current commit is not contained in any tag, GetVer takes the last version from the last tag that is parent of the current commit, not the last tag in the repository as it used to be
- (bugfix) fix binfile was never created
