# Beta Release v1.17.0
https://gitlab.cern.ch/api/v4/projects/74736/jobs/artifacts/refs/merge-requests%2F147%2Fhead/raw/changelog.md?job=changelog
## Repository info
- Merge request number: 147
- Branch name: 101-use-shared-runners-as-often-as-possible

## Changelog

- using shared runners when possible
