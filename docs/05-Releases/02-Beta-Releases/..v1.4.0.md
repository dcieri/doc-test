# Beta Release v1.4.0
https://gitlab.cern.ch/api/v4/projects/74736/jobs/artifacts/refs/merge-requests%2F130%2Fhead/raw/changelog.md?job=changelog
## Repository info
- Merge request number: 130
- Branch name: 95-smart-version

## Changelog

- Option to specify library for list files included in other list files using the lib=<mylib> property
- New function GetProjectVersion returns the last official tag when the projrect was modified or 0 if it was modified since last official tag
- pre-synthesis notices if the last commit for project is older than current commit
- when the repository is dirty the global SHA is still provided to the firmware, while the version is set to 0 ti signal a dirty repository
- new utility script to calculate last SHA in which a project was modified:
- GetVer now only takes a list of files as argument as well GetHash renamed GetSHA
- Update  and  templates with new generics
